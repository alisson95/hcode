<?php

namespace Hcode;

class Crypto {
    // Chave da Cripitografia
	const KEY_CRYPTO = "HcodePhp7_Secret";

	// Metodo da Cripitografia
	const CIPHER = "AES-128-CBC";
	
	public static function encrypt($text, $encodeUrl = false)
	{ 
		// Verifica se a string informada é um metodos de cripitografia válido
		if (in_array(Crypto::CIPHER, openssl_get_cipher_methods()))
		{
			$log = "\r\n Texto Original: $text \r\n";

		    $ivlen = openssl_cipher_iv_length(Crypto::CIPHER);
		    $log = $log . "ivlen: $ivlen \r\n";

		    $iv = openssl_random_pseudo_bytes($ivlen);
		    $log = $log . "iv: $iv \r\n";

		    $encryptedText = openssl_encrypt($text, Crypto::CIPHER, Crypto::KEY_CRYPTO, $options=0, $iv);
		    $log = $log . "Texto Cifrado: $encryptedText \r\n";

		    $textEncode64 = base64_encode($iv . $encryptedText);
		    $log = $log . "iv + Texto Cifrado em 64 : $textEncode64 \r\n";
		    
			if ($encodeUrl) $textEncode64 = urlencode($textEncode64);

			$log = $log . "cODIFICADO URL : $textEncode64 \r\n";


						$file = fopen("log.txt","a+");

						//"\r\n" pular linha
						fwrite($file,$log);

						fclose($file);



		    return $textEncode64;



		}// caso não seja tem que gerar erro
	}

	public static function decrypt($encryptedText)
	{   
	    $log =  "\r\n";
		$log = $log . "\r\n";

		$log = $log . "URL CODIFICADA: $encryptedText \r\n";
		$log = $log . "iv + Texto Cifrado em 64 : $encryptedText \r\n";

    	$textDecode64 = base64_decode($encryptedText);
    	$log = $log . "iv + Texto Cifrado sem 64: $textDecode64 \r\n";

		$ivlen = openssl_cipher_iv_length(Crypto::CIPHER);
		$log = $log . "ivlen: $ivlen \r\n";

		$iv = substr($textDecode64, 0, $ivlen);
 		$log = $log . "iv: $iv \r\n";

		$len = (strlen($textDecode64) - $ivlen);
		$textRaw = substr($textDecode64, ($ivlen), $len);
		$log = $log . "Texto Cifrado: $textRaw \r\n";

       	$decryptedText  = openssl_decrypt($textRaw, Crypto::CIPHER, Crypto::KEY_CRYPTO, $options=0, $iv);
       	$log = $log . "Texto Original: $decryptedText \r\n";



		$file = fopen("log.txt","a+");

		//"\r\n" pular linha
		fwrite($file,$log);

		fclose($file);

       	return $decryptedText;
	}
}


?>