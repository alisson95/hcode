<?php

use \Hcode\PageAdmin;
use \Hcode\Model\User;
use \Hcode\Model\Category;


$app->get('/admin/categories', function() {

	User::verifyLogin();

	$categories = Category::listAll();

	$page = new PageAdmin();

	$page->setTpl("categories", array(
		"categories"=>$categories
	));

});


$app->get('/admin/categories/create', function() {

	User::verifyLogin();

	$page = new PageAdmin();

	$page->setTpl("categories-create");

});

$app->post('/admin/categories/create', function() {

	User::verifyLogin();

	$category = new Category();

	$category->setData($_POST);

	$category->save();

	header("Location: /admin/categories");

	exit;
});

$app->get('/admin/categories/:idcatogory/delete', function($idcatogory) {

	User::verifyLogin();

	$category = new Category();

	$category->get((int)$idcatogory);

	$category->delete();

	header("Location: /admin/categories");

	exit;
});

$app->get('/admin/categories/:idcatogory', function($idcatogory) {

	User::verifyLogin();

	$category = new Category();

	$category->get((int)$idcatogory);
	
	$page = new PageAdmin();

	$page->setTpl("categories-update", array(
		"category"=>$category->getValues()
	));

});

$app->post('/admin/categories/:idcatogory', function($idcatogory) {

	User::verifyLogin();

	$category = new Category();

	$category->get((int)$idcatogory);
	
	$category->setData($_POST);

	$category->save();

	header("Location: /admin/categories");

	exit;
});

$app->get('/categories/:idcatogory', function($idcatogory) {

	User::verifyLogin();

	$category = new Category();

	$category->get((int)$idcatogory);
	
   $page = new Page();

   $page->setTpl("category", array(
   		'category'=>$category->getValues(),
   		'products'=>array(
   			"produto1"=>"teste",
   			"produto2"=>"teste"
   		)
   ));

});


?>